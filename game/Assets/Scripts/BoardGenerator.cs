﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Klasa generująca planszę do gry w statki
/// </summary>
public class BoardGenerator : MonoBehaviour
{
    /// <summary>
    /// Parametr GameObject generujący planszę do gry
    /// </summary>
    public GameObject BoardBox;

    /// <summary>
    /// Parametr int określający wielkość planszy do gry
    /// </summary>
    static private int boardSize = 10;
    /// <summary>
    /// Parametr float określający wielkość pola w grze
    /// </summary>
    private float boxSize = 10f;
    /// <summary>
    /// Tablica wielowymiarowa zawierająca
    /// </summary>
    GameObject[,] Board = new GameObject[boardSize, boardSize];
    //List<BoardBox> Selected = new List<BoardBox>();
     
    // Start is called before the first frame update
    /// <summary>
    /// Funkcja inicjalizująca, tworząca instancję obiektu, wywoływana na samym początku
    /// </summary>
    void Start()
    {
        for(int i = 0; i < boardSize; i++)
        {
            float x = i * boxSize;
            for (int j = 0; j < boardSize; j++)
            {
                float z = j * boxSize;
                Board[i, j] = Instantiate(BoardBox, this.transform.position + new Vector3(x, 0, z), BoardBox.transform.rotation, this.transform);
                Board[i, j].GetComponent<BoardBox>().SetBoardPosition(i, j);
            }
        }
    }

    // Update is called once per frame
    /// <summary>
    /// Funkcja aktualizująca, wywoływana po każdej klatce
    /// </summary>
    void Update()
    {
        
    }

    //public void ShowAvailableSelections(int x, int y, bool vertical_only = false, bool horizontal_only = false)
    //{
    //    //Board[x+1, y].GetComponent<BoardBox>().ShowSelection(true);
    //    //Board[x-1, y].GetComponent<BoardBox>().ShowSelection(true);
    //   // Board[x, y+1].GetComponent<BoardBox>().ShowSelection(true);
    //    //Board[x, y-1].GetComponent<BoardBox>().ShowSelection(true);
    //}

    //public bool ChceckAvailableHighlight(int x, int y)
    //{
    //    if (Selected.Count == 0)
    //    {
    //        return true;
    //    }
    //    else if(Selected.Count == 1)
    //    {
    //        ///if (Selected[0].CheckCoordinates()
    //        return true;
    //    }
    //    return false;
    //}

    //public void ResetSelection()
    //{
    //    Selected.Clear();
    //    foreach(GameObject go in Board)
    //    {
    //        go.GetComponent<BoardBox>().Deselect();
    //    }
    //}

    //public bool AddToSelected(BoardBox boardBox)
    //{
    //    if (Selected.Count == 1)
    //    {
    //        if (boardBox.x >= Selected[0].x - 1 && boardBox.x <= Selected[0].x + 1 && boardBox.y >= Selected[0].y - 1 && boardBox.y <= Selected[0].y + 1)
    //        {
    //            Selected.Add(boardBox);
    //            return true;
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }
    //    else if (Selected.Count > 1)
    //    {
    //        Debug.Log(Selected.Count);
    //        if (Selected[0].x == Selected[1].x && Selected[0].x == boardBox.x && boardBox.y >= Selected[Selected.Count - 1].y - 1 && boardBox.y <= Selected[Selected.Count - 1].y + 1)
    //        {
    //            Selected.Add(boardBox);
    //            return true;
    //        }
    //        else if (Selected[0].y == Selected[1].y && Selected[0].y == boardBox.y && boardBox.x >= Selected[Selected.Count -1].x - 1 && boardBox.x <= Selected[Selected.Count - 1].x + 1)
    //        {
    //            Selected.Add(boardBox);
    //            return true;
    //        }
    //        else
    //        {
    //            return false;
    //        }
    //    }

    //    Selected.Add(boardBox);
    //    return true;
    //}
}
