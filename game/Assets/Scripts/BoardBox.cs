﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Klasa generująca pole w planszy do gry
/// </summary>
public class BoardBox : MonoBehaviour
{/// <summary>
/// Parametr Material określający jak ma wyglądać dane pole (normalnie)
/// </summary>
    public Material Normal;
    /// <summary>
    /// Parametr Material określający jak ma wyglądać dane pole (jak przy najechaniu myszką)
    /// </summary>
    public Material Hover;
    /// <summary>
    /// Parametr Material określający jak ma wyglądać dane pole (jak przy zaznaczeniu ale troche inaczej)
    /// </summary>
    public Material SelectedHelp;
    /// <summary>
    /// Parametr Material określający jak ma wyglądać dane pole (jak przy zaznaczeniu)
    /// </summary>
    public Material Selected;
    /// <summary>
    /// Parametr MeshRender generujący siatkę mesh
    /// </summary>
    private MeshRenderer meshRenderer;
    /// <summary>
    /// Parametr BoardGenerator generujący planszę
    /// </summary>
    private BoardGenerator boardGenerator;

    /// <summary>
    /// Parametr int określający współrzędną x pola
    /// </summary>
    private int x_board;
    /// <summary>
    /// Parametr int określający współrzędną y pola
    /// </summary>
    private int y_board;
    /// <summary>
    /// Właściwość odczytująca parametr x_board <see cref="x_board"/>
    /// </summary>
    public int x
    {
        get
        {
            return x_board;
        }
    }
    /// <summary>
    /// Właściwość odczytująca parametr y_board <see cref="y_board"/>
    /// </summary>
    public int y
    {
        get
        {
            return y_board;
        }
    }
    /// <summary>
    /// Parametr bool sprawdzający czy pole jest podświetlone
    /// </summary>
    private bool isHighlighted = false;
    /// <summary>
    /// Parametr bool sprawdzający czy pole jest zaznaczone
    /// </summary>
    private bool isSelected = false;
    /// <summary>
    /// Parametr bool sprawdzający czy na danym polu jest statek
    /// </summary>
    private bool isShipOn = false;


    // Start is called before the first frame update
    /// <summary>
    /// Funkcja inicjalizująca, tworząca instancję obiektu, wywoływana na samym początku
    /// </summary>
    void OnEnable()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        boardGenerator = GetComponentInParent<BoardGenerator>();
    }

    // Update is called once per frame
    /// <summary>
    /// Funkcja aktualizująca, wywoływana po każdej klatce
    /// </summary>
    void Update()
    {
        if (isHighlighted && Input.GetMouseButton(0))
        {
            
            EnterSelectionMode();
        }
        if (Input.GetMouseButtonDown(1))
        {
            //boardGenerator.ResetSelection();
        }
    }
    /// <summary>
    /// Funkcja określająca zmianę pola po najechaniu myszką na nie
    /// </summary>
    private void OnMouseOver()
    {
        ShowSelection(true);
        isHighlighted = true;
    }
    /// <summary>
    /// Funkcja określająca zmianę pola po zjechaniu myszką z niego
    /// </summary>
    private void OnMouseExit()
    {
        ShowSelection(false);
        isHighlighted = false;
    }
    /// <summary>
    /// Funkcja ustawiająca pozycję pola
    /// </summary>
    /// <param name="x">współrzędna x pola</param>
    /// <param name="y">współrzędna y pola</param>
    public void SetBoardPosition(int x, int y)
    {
        this.x_board = x;
        this.y_board = y;
    }
    /// <summary>
    /// Funkcja która inicjalizuje tryb w którym można wybierać pola
    /// </summary>
    private void EnterSelectionMode()
    {
        //boardGenerator.ShowAvailableSelections(x_board, y_board);
        if (this.isSelected == false)
        {
            //if (boardGenerator.AddToSelected(this))
            //{
            //    this.isSelected = true;
            //}
        }
    }
    /// <summary>
    /// Funkcja pokazująca zaznaczone pole na planszy
    /// </summary>
    /// <param name="show">Paramter bool określający czy dane pole jest zaznaczone</param>
    public void ShowSelection(bool show)
    {
        if (!isSelected)
        {
            if (show)
            {
                meshRenderer.material = Selected;
            }
            else
            {
                meshRenderer.material = Normal;
            }
        }
    }
    /// <summary>
    /// Funkcja sprawdzająca współrzędne pola
    /// </summary>
    /// <param name="x">współrzędna x pola na planszy</param>
    /// <param name="y">współrzędna y pola na planszy</param>
    /// <returns></returns>
    public bool CheckCoordinates(int x, int y)
    {
        if (x_board == x && y_board == y)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    /// <summary>
    /// Funkcja odznaczająca wcześniej wybrany statek
    /// </summary>
    public void Deselect()
    {
        if (!isShipOn)
        {
            this.isSelected = false;
            ShowSelection(false);
        }
    }

}
