﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Klasa zawierająca definicję statku
/// </summary>
public class Ship : MonoBehaviour
{
    /// <summary>
    /// Parametr określający wielkość statku
    /// </summary>
    public int ShipSpace;

    // Start is called before the first frame update
    /// <summary>
    /// Funkcja inicjalizująca, tworząca instancję obiektu, wywoływana na samym początku
    /// </summary>
    void Start()
    {
        
    }

    // Update is called once per frame
    /// <summary>
    /// Funkcja aktualizująca, wywoływana po każdej klatce
    /// </summary>
    void Update()
    {
        
    }



}
