﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Klasa służąca do obsługi statków 
/// </summary>
public class ShipManager : MonoBehaviour
{
    /// <summary>
    /// Parametr Ship określający aktualnie wybrany statek
    /// </summary>
    public Ship SelectedShip = null;
    /// <summary>
    /// Parametr BoardManager obsługujący planszę do gry
    /// </summary>
    private BoardManager boardManager;
    // Start is called before the first frame update
    /// <summary>
    /// Funkcja inicjalizująca, tworząca instancję obiektu, wywoływana na samym początku
    /// </summary>
    void Start()
    {
        boardManager = FindObjectOfType<BoardManager>();
    }

    // Update is called once per frame
    /// <summary>
    /// Funkcja aktualizująca, wywoływana po każdej klatce
    /// </summary>
    void Update()
    {
        
    }
    /// <summary>
    /// Funckja zaznaczająca statek 
    /// </summary>
    /// <param name="ship"></param>
    public void SelectShip(Ship ship)
    {
        this.SelectedShip = ship;
    }
    /// <summary>
    /// Funkcja odznaczająca wcześniej wybrany statek
    /// </summary>
    public void DeselectShip()
    {
        this.SelectedShip = null;
    }

    //public bool CheckShipSpace()
    //{
    //    if (this.SelectedShip != null)
    //    {

    //    }
    //}
}
