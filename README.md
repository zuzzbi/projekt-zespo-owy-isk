---------------
| GRA W STATKI |
---------------

# Projekt Zespoowy ISK

JAK SIE POLACZYC, ABY ROZPOCZAC GRE:
Po wlaczeniu aplikacji uzytkownikowi wyswietla sie ekran glowny.
Aby rozpoczac gre, trzeba najpierw ustalic ID gracza.
Nastepnie wybrac pokoj gry.
Klik aby rozpoczac gre


GRA W STATKI:
Gra odbywa sie w turach i polega na zestrzeleniu wszystkich statkow przeciwnika zanim on zestrzeli nasze.

Na pocztku kady z graczy ma swoj plansz gdzie ustawia swoje statki.
Statkow jest kilka wielkoci (2-3-4 pola) i mozna je ustawiac tylko pionowo lub poziomo na planszy.
Gracze na poczatku maja do ustawienia statki po czym klikaja rozpocznij.
Gracze w turach oddaja strzaly klikajac w plansze przeciwnika. Jesli strzal trafi to mozna wykonac kolejny strzal ale jeli spudluje to ich tura dobiega konca.
Gra konczy sie w momencie, gdy wszystkie statki jednego z graczy zostana zestrzelone.
Wynik gry jest zapisywany, ranking graczy mozna zobaczyc na stronie:
http://projektzespolowy.livs.pl/




Tworzenie pokoju 
Aby stworzyć pokój użyj zapytania POST dla '/api/room/create_room' podając dane w JSON.
Należy podać 4 parametry podane w tym przykładzie:
{
	"player1_id" : 1,
	"player2_id" : 2,
	"player1_board" : "{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}",
	"player2_board" : "{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}"
}
ID nowego pokoju jest przydzielane automatycznie.
Gracz po utworzeniu nowego pokoju dostanie w odpowiedzi id tego pokoju.

Uzyskanie unikalnego id gracza
Najpierw musimy sprawdzić, czy gracz istnieje już w bazie danych. Aby to sprawdzić, użyj zapytania GET dla '/api/players/<player_name>/id'. W miejsce <player_name> wpisz nick gracza.
Jeśli nie istnieje, zostanie zwrócony pusty string. Jeśli istnieje, zwrócone zostanie przydzielone mu wcześniej id.
Jeśli gracz nie istnieje, można go dodać za pomocą zapytania POST dla '/api/players/add_player' podając dane w JSON tak jak w przykładzie:
{
	"name": "nazwa_gracza"
}

Uzyskanie informacji o ruchu gracza
Aby otrzymać informację o ruchu gracza użyj zapytania GET dla '/api/room/<room_id>/player1_move' dla gracza 1 lub '/api/room/<room_id>/player2_move' dla gracza 2. W miejsce <room_id> wpisz id pokoju.



Zmienne oraz ich typy w naszej bazie danych
TABELA "rooms"
room_id - int (id pokoju)
player1_id - int (id gracza 1)
player2_id - int (id gracza 2)
player1_board - text (informacja o planszy gracza 1)
player2_board - text (informacja o planszy gracza 2)
player1_move - text (informacja o ruchu gracza 1)
player2_move - text (informacja o ruchu gracza 2)
TABELA "players"
id - int (unikalne id gracza)
name - text (nick gracza)
won - int (ilość wygranych gier)
lost - int (ilość przegranych gier)


GET

GET /api/room/<room_id>/player1_id - zwraca id gracza 1 w danym pokoju
Przykładowy zwracany JSON:
{"player1_id": 3}

GET /api/room/<room_id>/player2_id - zwraca id gracza 2 w danym pokoju
Przykładowy zwracany JSON:
{"player2_id": 2}

GET /api/room/<room_id>/player1_board - zwraca informacje o planszy gracza 1 w danym pokoju
Przykładowy zwracany JSON:
{"player1_board": "{1,0,0,1,0,0,0,0,0,0}"}

GET /api/room/<room_id>/player2_board - zwraca informacje o planszy gracza 2 w danym pokoju
Przykładowy zwracany JSON:
{"player2_board": "{1,1,0,0,0,0,0,0,0,0}"}

GET /api/room/<room_id>/player1_move - zwraca informacje o ruchu gracza 1 w danym pokoju
Przykładowy zwracany JSON:
{"player1_move": "{0,1}"}

GET /api/room/<room_id>/player2_move - zwraca informacje o ruchu gracza 2 w danym pokoju
Przykładowy zwracany JSON:
{"player2_move": "{1,0}"}

GET /api/room_ids - zwraca id wszystkich istniejących pokojów
Przykładowy zwracany JSON:
{"room_ids": [1, 2, 3, 4, 5, 6, 7, 8, 9, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]}

GET /api/rooms - wywala wszystkie informacje o wszystkich pokojach

GET /api/room_last - zwraca id ostatnio utworzonego pokoju
Przykładowy zwracany JSON:
{"room": 31}

GET /api/players/<player_name>/id - zwraca id gracza o podanym nicku
Przykładowy zwracany JSON:
{"id": 1}






PATCH

PATCH /api/room/<room_id>/move - uzupełnia pola player1_move oraz pola2_move
Potrzebny JSON:
{
	"player1_move" : "{1,2}",
	"player2_move" : "{2,3}"
}

PATCH /api/room/<room_id>/board - uzupełnia pola player1_board oraz pola2_board
Potrzebny JSON:
{
	"player1_board" : "{0,1,0,0,1,1,1,1,0,1,0,1}",
	"player2_board" : "{1,0,0,0,0,1,1,1,1,1,0,0}"
}

PATCH /api/room/<room_id>/player2_id – zmienić player2_id
Potrzebny JSON:
{
	" player2_id " :  5
}
PATCH /api/players/<id>/player_won– dodać +1 do tabeli won
Json nie jest potrzebny.
PATCH /api/players/<id>/player_lost– dodać +1 do tabeli lost
Json nie jest potrzebny.

POST

POST /api/room/create_room - tworzy nowy pokój
Potrzebny JSON:
{
	"player1_id" : 1,
	"player2_id" : 2,
	"player1_board" : "{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}",
	"player2_board" : "{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}"
}
W przypadku sukcesu, otrzymamy JSON:
{"new-room-id": 32}

POST/api/players/add_player - dodaje do bazy nowego gracza
Potrzebny JSON:
{
	"name” : “nazwa_nowego_gracza”
}
W przypadku sukcesu otrzymamy JSON:
{"new-player-id": 6}















Credits:
- Zuzanna bikowska
- Micha Bogusawski
- Inga Jakacka
- Sebastian yszkowski
- Elena Yermak
- Egor Kozak
- Jerzy Chrupek
- Kacper Kruszewski
- Micha Stocki
- ukasz Stanicki
- Pawe Wgiel
- Marek Bobrowski
- Mateusz Gobiewski
- Pavel Shyshko
- Marcin Kurach 
- Marcin Moko


