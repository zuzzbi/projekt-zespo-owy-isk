var searchData=
[
  ['board_0',['Board',['../class_board_generator.html#af1e533b26134d6673475a81a6f666f5d',1,'BoardGenerator']]],
  ['boardbox_1',['BoardBox',['../class_board_box.html',1,'BoardBox'],['../class_board_generator.html#af3914ccce0d50611760a949b877aef02',1,'BoardGenerator.BoardBox()']]],
  ['boardbox_2ecs_2',['BoardBox.cs',['../_board_box_8cs.html',1,'']]],
  ['boardgenerator_3',['BoardGenerator',['../class_board_generator.html',1,'BoardGenerator'],['../class_board_box.html#ac0fe44735207171936911e18052cf3db',1,'BoardBox.boardGenerator()']]],
  ['boardgenerator_2ecs_4',['BoardGenerator.cs',['../_board_generator_8cs.html',1,'']]],
  ['boardmanager_5',['BoardManager',['../class_board_manager.html',1,'BoardManager'],['../class_ship_manager.html#aaae19cda5c803f6e890a143641d16cce',1,'ShipManager.boardManager()']]],
  ['boardmanager_2ecs_6',['BoardManager.cs',['../_board_manager_8cs.html',1,'']]],
  ['boardsize_7',['boardSize',['../class_board_generator.html#a5eacf4f1a4fa8c1fd8f7f807ae593e4d',1,'BoardGenerator']]],
  ['boxsize_8',['boxSize',['../class_board_generator.html#a6f08157a128c3988f826b8e99e5cc586',1,'BoardGenerator']]]
];
